package pattern;

/*
    Interface that provides a way to create objects
    implementing the Animal interface
 */
public interface AbstractFactory {
    public Animal createObject();
}
