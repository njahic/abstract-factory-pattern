package pattern;

/*
    Concrete implementation of the abstract factory
    which creates Cat objects
 */
public class CatFactory implements AbstractFactory {
    @Override
    public Animal createObject() {
        return new Cat();
    }
}
