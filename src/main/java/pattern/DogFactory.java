package pattern;

/*
    Concrete implementation of the abstract factory
    which creates Dog objects
 */
public class DogFactory implements AbstractFactory {
    @Override
    public Animal createObject() {
        return new Dog();
    }
}
